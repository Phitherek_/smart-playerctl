# CHANGELOG

## v. 0.1.0

* Initial release

## v. 0.2.0

* Properly handle empty players list
* Handle selected player not being included in players list

## v. 0.2.1

* Small fixes in install scripts

## v. 0.2.2

* Small fixes in install scripts
