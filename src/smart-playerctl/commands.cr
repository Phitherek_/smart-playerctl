require "libnotify"

module SmartPlayerctl
  class Commands
    def self.execute(cmd, params = [] of String)
      case cmd
      when "help"
        print_help
      when "version"
        print_version
      when "list-players"
        list_players
      when "select-player"
        select_player(params[0]?)
      when "next-player"
        next_player
      when "previous-player"
        previous_player
      when "current-player"
        current_player
      when "play", "pause", "play-pause", "stop", "next", "previous", "status", "position", "volume", "metadata", "open", "loop", "shuffle"
        execute_playerctl_command(cmd, params)
      else
        print_help
      end
    end

    private def self.print_help
      puts "Usage:"
      puts "#{PROGRAM_NAME} COMMAND - A smart playerctl wrapper"
      puts
      puts "Available commands:"
      puts " help - print this text"
      puts " list-players - list available players"
      puts " select-player [PLAYER] - select specific player as current player"
      puts " next-player - select next player from the list (or first if it is not found) as current player"
      puts " previous-player - select previous player from the list (or first if it is not found) as current player"
      puts " current-player - show current player"
      puts " play - send playerctl play command to current player"
      puts " pause - send playerctl pause command to current player"
      puts " play-pause - send playerctl play-pause command to current player"
      puts " stop - send playerctl stop command to current player"
      puts " next - send playerctl next command to current player"
      puts " previous - send playerctl previous command to current player"
      puts " status - get current player status from playerctl"
      puts " position [OFFSET][+/-] - send playerctl offset command to current player"
      puts " volume [LEVEL][+/-] - send playerctl volume command to current player"
      puts " metadata [KEY...] - send playerctl metadata command to current player"
      puts " open [URI] - send playerctl open command to current player"
      puts " loop [STATUS] - send playerctl loop command to current player"
      puts " shuffle [STATUS] - send playerctl status command to current player"
      puts
      puts "See playerctl help for more info about playerctl commands"
    end

    private def self.print_version
      puts SmartPlayerctl::VERSION
    end

    private def self.list_players
      players_list = get_players_list
      print players_list.join("\n")
      puts
    end

    private def self.select_player(player)
      if player.nil?
        print_help
        return
      end

      players_list = get_players_list
      if players_list.includes?(player)
        Config.new.write(player)
        puts player
        player_switch_notification(player)
      else
        puts "Player does not exist!"
      end
    end

    private def self.next_player
      players_list = get_players_list
      current_player = get_current_player

      idx = players_list.index(current_player)
      if idx.nil?
        idx = -1
      end

      idx += 1
      idx = 0 if idx >= players_list.size
      Config.new.write(players_list[idx])
      puts players_list[idx]
      player_switch_notification(players_list[idx])
    end

    private def self.previous_player
      players_list = get_players_list
      current_player = get_current_player

      idx = players_list.index(current_player)
      if idx.nil?
        idx = players_list.size
      end

      idx -= 1
      idx = players_list.size - 1 if idx < 0
      Config.new.write(players_list[idx])
      puts players_list[idx]
      player_switch_notification(players_list[idx])
    end

    private def self.current_player
      puts get_current_player
    end

    private def self.get_current_player
      players_list = get_players_list
      current_player = Config.new.read
      current_player = nil unless players_list.includes?(current_player)
      current_player
    end

    private def self.get_players_list
      players_list = `playerctl -l`.chomp
      players_list = players_list.split("\n").compact
      players_list.delete("")
      players_list
    end

    private def self.execute_playerctl_command(command, args = [] of String)
      current_player = get_current_player
      cmd = "playerctl"
      cmd += " -p #{current_player}" if !current_player.nil? && current_player != ""
      cmd += " #{command} #{args.join(" ")}"
      output = `#{cmd}`
      print output
    end

    private def self.player_switch_notification(player)
      Libnotify.show do |notify|
        notify.summary = "smart-playerctl player switched"
        notify.body = "to: #{player}"
        notify.urgency = :normal
        notify.timeout = 5.0
      end
    end
  end
end
