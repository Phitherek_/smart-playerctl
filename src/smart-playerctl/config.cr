module SmartPlayerctl
  class Config
    def initialize
      @dir = "#{ENV["HOME"]}/.smart-playerctl"
      @path = (@dir + "/player").as(String)
    end

    def read
      File.read(@path)
    rescue File::Error
      nil
    end

    def write(value)
      Dir.mkdir_p(@dir)
      File.write(@path, value)
    end
  end
end
