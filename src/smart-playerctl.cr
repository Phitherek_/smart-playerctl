require "./smart-playerctl/*"
module SmartPlayerctl
  VERSION = "0.2.2"
end

if ARGV.size == 1
  SmartPlayerctl::Commands.execute(ARGV[0])
elsif ARGV.size > 1
  SmartPlayerctl::Commands.execute(ARGV[0], ARGV[1..])
else
  SmartPlayerctl::Commands.execute("help")
end
