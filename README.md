# smart-playerctl

A smart wrapper for playerctl

## Installation

`./build.sh` - to build for release

`sudo PREFIX=/usr/local ./install.sh` - to install in the system, if PREFIX is not given it defaults to /usr/local

## Usage

`smart-playerctl help` for usage instructions

## Dependencies

* libnotify
* playerctl

## Development

See Contributing section.

Remember to use `bin/ameba` to lint your code before contributing.

This project is tested manually (I know, I know, I just can't bother with something this simple).

## Contributing

1. Fork it (<https://gitlab.com/Phitherek_/smart-playerctl/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [Phitherek_](https://gitlab.com/Phitherek_) - creator and maintainer
